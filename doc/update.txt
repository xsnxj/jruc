版本1.1更新说明：

1、增加阿里云OSS支持，并添加ueditor阿里云OSS支持
2、增加阿里云短信支持
3、数据字典添加公共方法
4、shiro添加redis集群支持

版本1.2更新说明：

1、升级jfinal到3.1版本，升级beetl到2.7.14
2、升级druid到1.0.29，升级数据库密码加密方式，增加druidsql打印
3、整合shiro/activerecord缓存，支持redis/ehcache一键配置