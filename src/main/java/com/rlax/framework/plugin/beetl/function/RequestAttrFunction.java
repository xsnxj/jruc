package com.rlax.framework.plugin.beetl.function;

import org.beetl.core.Context;
import org.beetl.core.Function;

/**
 * 在标签里输出 request attr 的值
 * 
 * @author Rlax
 * 
 */
public class RequestAttrFunction implements Function {

	@Override
	public Object call(Object[] paras, Context ctx) {
		Object o = paras[0];
		Object value = "";
		if (o != null) {
			value = ctx.getGlobal(o.toString());
			if (value == null) {
				value = "";
			}
		}
		return value.toString();
	}

}
