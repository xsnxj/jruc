package com.rlax.framework.plugin.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 抽象JOB
 * @author Rlax
 *
 */
public abstract class AbstractJob implements Job {

	@Override
	public void execute(JobExecutionContext jcx) throws JobExecutionException {
		preExecute(jcx);
		try {
			nativeExecute(jcx);
		} catch (Exception e) {
			e.printStackTrace();
		}
		afterExecute(jcx);
	}

	/**
	 * JOB执行之前
	 * 
	 * @param context
	 */
	protected abstract void preExecute(JobExecutionContext context);

	/**
	 * JOB执行
	 * 
	 * @param context
	 * @throws Exception 
	 */
	protected abstract void nativeExecute(JobExecutionContext context) throws Exception;

	/**
	 * JOB执行之后
	 * 
	 * @param context
	 */
	protected abstract void afterExecute(JobExecutionContext context);

}
