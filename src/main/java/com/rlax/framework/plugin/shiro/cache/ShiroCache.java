package com.rlax.framework.plugin.shiro.cache;

import java.util.Collection;
import java.util.Set;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;

import com.rlax.framework.support.cache.CacheExtKit;

/**
 * Shiro 缓存，封装 redis/ehcache
 * @author Rlax
 *
 * @param <K>
 * @param <V>
 */
public class ShiroCache<K, V> implements Cache<K, V> {

	public ShiroCache(String cacheName) {
		this.cacheName = cacheName;
	}
	
	private String cacheName;
	
	@Override
	public V get(K key) throws CacheException {
		return CacheExtKit.get(cacheName, key);
	}

	@Override
	public V put(K key, V value) throws CacheException {
		CacheExtKit.put(cacheName, key, value);
		return value;
	}

	@Override
	public V remove(K key) throws CacheException {
		V value = CacheExtKit.get(cacheName, key);
		CacheExtKit.remove(cacheName, key);
		return value;
	}

	@Override
	public void clear() throws CacheException {
		CacheExtKit.removeAll(cacheName);		
	}

	@Override
	public int size() {
		return CacheExtKit.size(cacheName);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<K> keys() {
		return (Set<K>) CacheExtKit.keys(cacheName);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<V> values() {
		return (Collection<V>) CacheExtKit.values(cacheName);
	}

}
