package com.rlax.framework.support.entity;

/**
 * 消息实体
 * 
 * @author Rlax
 * 
 */
public class WeixinMessageEntity {

	/** 标题 */
	private String title;
	/** 消息内容 */
	private String message;
	/** 1：success 2：warn 3：info */
	private String type;

	/** 按钮url */
	private String buttonUrl;
	/** 按钮内容 */
	private String buttonValue;

	public void success(String title, String message, String url, String value) {
		this.message = message;
		this.type = "success";
		this.title = title;
		this.buttonUrl = url;
		this.buttonValue = value;
	}

	public void warn(String title, String message, String url, String value) {
		this.message = message;
		this.type = "warn";
		this.title = title;
		this.buttonUrl = url;
		this.buttonValue = value;
	}
	
	public void info(String title, String message, String url, String value) {
		this.message = message;
		this.type = "info";
		this.title = title;
		this.buttonUrl = url;
		this.buttonValue = value;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getButtonUrl() {
		return buttonUrl;
	}

	public void setButtonUrl(String buttonUrl) {
		this.buttonUrl = buttonUrl;
	}

	public String getButtonValue() {
		return buttonValue;
	}

	public void setButtonValue(String buttonValue) {
		this.buttonValue = buttonValue;
	}
	
}