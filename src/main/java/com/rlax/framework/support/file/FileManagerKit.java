package com.rlax.framework.support.file;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 文件管理工具类
 * @author Rlax
 *
 */
public class FileManagerKit {

	private static FileManager fileManager = null;
	private static final ConcurrentHashMap<String, FileManager> map = new ConcurrentHashMap<String, FileManager>();

	private FileManagerKit() {}
	
	public static FileManager use(String fileManagerName) {
		fileManager = get(fileManagerName);
		return fileManager;
	}
	
	public static void add(FileManager fileManager, String fileManagerName) {
		map.put(fileManagerName, fileManager);
	}
	
	public static FileManager get(String fileManagerName) {
		return map.get(fileManagerName);
	}
	
	/**
	 * 文件上传
	 * @param file
	 * @param rootPath
	 * @param savePath
	 * @return
	 */
	public static FileResult save(File file, String savePath) {
		return fileManager.save(file, savePath);
	}
	
	/**
	 * 保存二进制文件
	 * @param data 图片二进制信息
	 * @param rootPath 跟路径
	 * @param savePath 保存路径
	 * @return state 状态接口
	 */
	public static FileResult save(byte[] data, String savePath) {
		return fileManager.save(data, savePath);
	}
	
	/**
	 * 保存流文件
	 * @param is 流
	 * @param rootPath 跟路径
	 * @param savePath 保存路径
	 * @return state 状态接口
	 */
	public static FileResult save(InputStream is, String savePath) {
		return fileManager.save(is, savePath);
	}
	
	/**
	 * 获取前缀下的文件信息
	 * @param dirPath 前缀
	 * @return
	 */
	public static List<FileInfo> list(String dirPath) {
		return fileManager.list(dirPath);
	}
	
	/**
	 * 获取前缀下的文件信息
	 * @return
	 */
	public static List<FileInfo> list() {
		return fileManager.list();
	}
	
	/** 
	 * 获取文件下载endpoint
	 * @return
	 */
	public static String getDownloadEndpoint() {
		return fileManager.getDownloadEndpoint();
	}
	
	/**
	 * 根据文件key获取私有文件下载地址
	 * @param key 存储文件key
	 * @return
	 */
	public static String getDownPathByKey(String key) {
		return fileManager.getDownPathByKey(key);
	}
}
