package com.rlax.framework.support.file.oss;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * oss 上传成功后 回调控制器
 * @author Rlax
 *
 */
public class OssCallBackController {

	private static final Logger logger = LoggerFactory.getLogger(OssCallBackController.class);

}
