package com.rlax.framework.util;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;

/**
 * TreeKit
 * @author Rlax
 *
 */
public class TreeKit {
	
	/***
	 * 把 model 转化为 list 找到其中的单个属性
	 * 
	 * @param sql
	 * @param attr
	 * @return
	 */
	public static List<String> getAttr(Model<? extends Model>  dao, String sql, String attr, Object ... param) {
		List<String> list = new ArrayList<String>();
		for (Model t : dao.find(sql, param)) {
			list.add(t.getStr(attr));
		}
		return list;
	}

}
