package com.rlax.web.base;

import java.io.Serializable;

/**
 * 提示消息 alert
 * @author Rlax
 *
 */
public class MessageBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public final static String SUCCESS_TYPE = "success";
	public final static String INFO_TYPE = "info";
	public final static String DANGER_TYPE = "danger";
	
	public static final String SUCCESS_MSG = "操作成功";
	public static final String ERROR_MSG = "操作失败";
	
	private String msg;
	private String type;

	public MessageBean() {
	}
	
	public MessageBean(String msg, String type) {
		this.msg = msg;
		this.type = type;
	}
	
	public void success(String msg) {
		this.type = SUCCESS_TYPE;
		this.msg = msg;
	}

	public void success() {
		this.type = SUCCESS_TYPE;
		this.msg = SUCCESS_MSG;
	}

	public void error(String msg) {
		this.type = DANGER_TYPE;
		this.msg = msg;
	}

	public void error() {
		this.type = DANGER_TYPE;
		this.msg = ERROR_MSG;
	}	

	public void info(String msg) {
		this.type = INFO_TYPE;
		this.msg = msg;
	}
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
