package com.rlax.web.controller;

import java.util.Date;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;

import com.jfinal.aop.Before;
import com.rlax.framework.common.Consts;
import com.rlax.framework.common.Status;
import com.rlax.web.base.BaseController;
import com.rlax.web.model.User;
import com.rlax.web.validator.LoginValidator;
import com.rlax.web.validator.UserValidator;

/**
 * Index
 * @author Rlax
 *
 */
public class IndexController extends BaseController {
	
	public void index() {
		render("index.html");
	}
	
	public void login() {
		if (SecurityUtils.getSubject().isAuthenticated()) {
			redirect("/");
		} else {
			render("login.html");
		}
	}
	
	public void captcha() {
		renderCaptcha();
	}
	
	@Before(LoginValidator.class)
	public void do_login() {
		String loginName = getPara("loginName");
		String pwd = getPara("password");
		
		UsernamePasswordToken token = new UsernamePasswordToken(loginName, pwd);
		Subject subject = SecurityUtils.getSubject();
		
		boolean flag = false;
		try {
			if (!subject.isAuthenticated()) {
				token.setRememberMe(false);
				subject.login(token);
				User u = User.dao.findByName(loginName);
				subject.getSession(true).setAttribute(Consts.SESSION_USER, u);
			}
			if (getParaToBoolean("rememberMe") != null && getParaToBoolean("rememberMe")) {
				setCookie("loginName", loginName, 60 * 60 * 24 * 7);
			} else {
				removeCookie("loginName");
			}
		} catch (UnknownAccountException une) {
			keepPara("loginName", "password");
			setAttr("loginNameMsg", "用户名不存在");
			flag = true;
		} catch (IncorrectCredentialsException ine) {
			keepPara("loginName", "password");
			setAttr("loginNameMsg", "用户名密码错误");		
			flag = true;
		} catch (Exception e) {
			keepPara("loginName", "password");
			setAttr("loginNameMsg", "服务器异常，请稍后重试");	
			flag = true;
		}
		
		if (flag) {
			render("login.html");
		} else {
			String backUrl = "/";
			SavedRequest saveRequest = WebUtils.getAndClearSavedRequest(getRequest());
			if (saveRequest != null && saveRequest.getMethod().equalsIgnoreCase(AccessControlFilter.GET_METHOD)) {
				backUrl = saveRequest.getRequestUrl();
			}
			redirect(backUrl);
		}
	}
	
	/**
	 * 注册
	 */
	public void register() {
		if (SecurityUtils.getSubject().isAuthenticated()) {
			redirect("/");
		} else {
			render("register.html");
		}
	}
	
	/**
	 * 注册操作
	 */
	@Before(UserValidator.class)
	public void do_register() {
		User user = getModel(User.class);
		user.setStatus(Status.USER_REGISTER);
		user.setCreatedate(new Date());
		user.encrypt().save();
	}
	
	public void logout() {
		if (SecurityUtils.getSubject().isAuthenticated()) {
			Subject subject = SecurityUtils.getSubject();
			subject.logout();
			redirect("/");
		} else {
			redirect("/");
		}
	}

	/**
	 * ueditor 测试
	 */
	public void ueditor() {
		render("ueditor.html");
	}
	
	@Override
	public void onExceptionError(Exception e) {
		render("login.html");
	}
}