package com.rlax.web.controller;

import java.util.Date;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.rlax.framework.common.Consts;
import com.rlax.framework.common.Status;
import com.rlax.web.base.BaseController;
import com.rlax.web.model.User;
import com.rlax.web.validator.UserValidator;

/**
 * 用户管理
 * @author Rlax
 *
 */
//@ControllerBind(controllerKey = "/system/user")
public class UserController extends BaseController {

	public void index() {
		int page = getParaToInt("_page", 1);
		User info = new User();
		info.setName(getPara("name"));
		info.setEmail(getPara("email"));
		
		Page<User> list = User.dao.findPage(info, page, Consts.PAGE_DEFAULT_SIZE);
		keepPara();
		setAttr("page", list);
		render("main.html");
	}
	
	public void add() {
		render("add.html");
	}

	@Before(UserValidator.class)
	public void do_add() {
		User user = getModel(User.class);
		user.setStatus(Status.USER_REGISTER);
		user.setCreatedate(new Date());
		user.encrypt().save();
		redirect("/system/user");
	}
	
	public void update() {
		render("update.html");
	}	
	
	@Override
	public void onExceptionError(Exception e) {
		// TODO Auto-generated method stub
	}

}
