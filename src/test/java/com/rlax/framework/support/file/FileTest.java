package com.rlax.framework.support.file;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.AccessControlList;

public class FileTest {

	public static void main(String[] args) {
		OSSClient client = new OSSClient("http://oss-cn-beijing.aliyuncs.com", "xxx", "xxx");
		 AccessControlList acl = client.getBucketAcl("oss");
		System.out.println(acl.toString());
		System.out.println(acl.getCannedACL().toString());
	}
}
